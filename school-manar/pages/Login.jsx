import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {View, Text, TextInput,Image, TouchableOpacity, Alert, ActivityIndicator, KeyboardAvoidingView} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { login, setData } from '../redux/AuthSlice';
import styles from '../styles/LoginStyle';
import { ScrollView } from 'react-native';
import { Button } from 'react-native-paper';
import Loading from './Loading';




const Login = ({navigation}) => {
  const [username, setUsername] = useState('');
  const [password, setPass] = useState('');
  const [loading,setLoading]=useState(false);
  const [error, setError] = useState('');
  const loginError=useSelector(state=>state.auth.error);
  const dispatch=useDispatch();
  useEffect(()=>{
    if(error){
      alert(error);
    }
  },[error]);
  const sendCred = async ()=>{
    if(!username||!password){
      setError("plaise fill all fields ")
    }
    else{
      
      setError("");
      //let exist=false;
      //dispatch(login({name:username,password}))
      
    setLoading(true);
    fetch("https://44aa-169-150-196-78.ngrok-free.app/api/auth/login",{
      method:"POST",
      headers: {
       'Content-Type': 'application/json'
     },
     body:JSON.stringify({
       "name":username,
       "password":password
     })
    })
    .then(res=>res.json())
    .then(async (data)=>{
           try {
             if (data.error){
              setError('invalid username or password');
              //console.log(data)
              setLoading(false)
             }
             else if (data.name || data.password){
              setError("plaise fill all fields ")
              //console.log(data)
              setLoading(false)
             }
             else{
              await AsyncStorage.setItem('data',JSON.stringify(data));
              dispatch(setData(data));
              console.log(data.user)
              setLoading(false);
             }
             
           } catch (e) {
             console.log("error hai",e)
              //Alert(e)
           }
      
    })
    .catch(err=>{
      setLoading(false);
      console.log(err);
    })
    }
    
      
 }

 if(loading){
  return <Loading  />
 }
  return (
    <KeyboardAvoidingView behavior="height" enabled style={{ flex: 1 }}>
      
        <ScrollView
          contentContainerStyle={{
            flexGrow: 1,
          }}
        >
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor:  "white",
            }}
          >
            <Image
              resizeMode="contain"
              style={{
                height: 220,
                width: 220,
              }}
              source={require("../assets/login.png")}
            />
          </View>
          <View
            style={{
              flex: 3,
              paddingHorizontal: 20,
              paddingBottom: 20,
              backgroundColor:  "white",
            }}
          >
            <Text
              fontWeight="bold"
              style={styles.title}
              size="h3"
            >
              Login
            </Text>
            <Text
              style={styles.inputLable}
            >
              Username
            </Text>
            <TextInput
              style={styles.txtEmail}
              placeholder="Enter your username"
              value={username}
              autoCapitalize="none"
              autoCompleteType="off"
              autoCorrect={false}
              keyboardType="email-address"
              onChangeText={(text) => setUsername(text)}
            />

            <Text style={styles.inputLable}>Password</Text>
            <TextInput
              style={styles.txtPass}
              placeholder="Enter your password"
              value={password}
              autoCapitalize="none"
              autoCompleteType="off"
              autoCorrect={false}
              secureTextEntry={true}
              onChangeText={(text) => setPass(text)}
            />
            <TouchableOpacity
              onPress={() => sendCred()}
              style={styles.btnLogin}>
              <Text style={styles.btnCaption}>Login</Text>
            </TouchableOpacity>

            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginTop: 15,
                justifyContent: "center",
              }}
            >
            </View>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginTop: 10,
                justifyContent: "center",
              }}
            >
                {/* <TouchableOpacity
                  onPress={() => {
                    navigation.navigate("ForgetPassword");
                  }}
                >
                  <Text size="md" fontWeight="bold">
                    Forget password
                  </Text>
                </TouchableOpacity> */}
            </View>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginTop: 30,
                justifyContent: "center",
              }}
            >
              {/* <TouchableOpacity
                onPress={() => {
                  true ? setTheme("light") : setTheme("dark");
                }}
              >
                <Text
                  size="md"
                  fontWeight="bold"
                  style={{
                    marginLeft: 5,
                  }}
                >
                  {true ? "☀️ light theme" : "🌑 dark theme"}
                </Text>
              </TouchableOpacity> */}
            </View>
          </View>
        </ScrollView>
      
    </KeyboardAvoidingView>
    // <View style={styles.parent}>
    //   <Text style={styles.title}>Login</Text>

    //   <View style={styles.errView}>
    //     <Text style={styles.err}>{error}</Text>
    //   </View>

    //   <View style={styles.input}>
    //     <TextInput
    //       placeholder="username"
    //       value={username}
    //       onChangeText={e => setUsername(e)}
    //       style={styles.txtEmail}
    //     />
    //     <TextInput
    //       placeholder="Password"
    //       value={password}
    //       onChangeText={e => setPass(e)}
    //       style={styles.txtPass}
    //       secureTextEntry={true}
    //     />
    //     <View style={styles.passView}>
    //       <Text style={styles.forgotPass}>Forgot Password? </Text>
    //     </View>
    //   </View>

    //   <View style={styles.modal}>
    //     <TouchableOpacity
    //       onPress={() => sendCred()}
    //       style={styles.btnLogin}>
    //       <Text style={styles.btnCaption}>Login</Text>
    //     </TouchableOpacity>

    //     <View style={styles.no_acc}>
    //       <Text style={styles.acc_txt}>Don't have an account?</Text>
    //       <Text
    //         style={styles.reg}
    //         onPress={() => navigation.navigate('signup')}>
    //         Create one here
    //       </Text>
    //     </View>
    //   </View>
    // </View>
  );
};

export default Login;
