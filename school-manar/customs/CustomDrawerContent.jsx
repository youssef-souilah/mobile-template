import { DrawerContentScrollView, DrawerItemList, useDrawerProgress, useDrawerStatus } from "@react-navigation/drawer";
import { TouchableOpacity, View,Image ,Text} from "react-native";
import Animated from "react-native-reanimated";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import profile from "../assets/login.png"
import { useDispatch, useSelector } from "react-redux";
import { userLogout } from "../redux/AuthSlice";

export default function CustomDrawerContent(props) {

  const {user}=useSelector(state=>state.auth);
  const dispatch=useDispatch();
    const isDrawerOpen = useDrawerStatus();
    const progress = useDrawerProgress();
    const scale = Animated.interpolateNode(progress, {
      inputRange: [0, 1],
      outputRange: [1, 0.8],
  
    });
    const borderRadius = Animated.interpolateNode(progress, {
      inputRange: [0, 1],
      outputRange: [0, 25],
    });
  
  //data.profile
  
    return (user&&
      <DrawerContentScrollView {...props}
      style={{flexDirection:"row",}}>
        <View style={{ justifyContent: 'flex-start', padding: 15 }}>
          <Image source={{ uri: "https://imgs.search.brave.com/bGvmpgkWdt1O0-I-e7NIZ5nKy-rAWcCTfrmvRi6ENrw/rs:fit:437:225:1/g:ce/aHR0cHM6Ly90c2Ux/Lm1tLmJpbmcubmV0/L3RoP2lkPU9JUC5H/bFhxeGNSOUVtdmlO/NWt1d2FVc01RSGFJ/QiZwaWQ9QXBp" }} style={{
            width: 60,
            height: 60,
            borderRadius: 10,
            marginTop: 8
          }}></Image>
  
          <Text style={{
            fontSize: 20,
            fontWeight: 'bold',
            color: 'white',
            marginTop: 20
          }}>{user.name}</Text>
  
          <TouchableOpacity>
            <Text style={{
              marginTop: 6,
              color: 'white'
            }}>View Profile</Text>
          </TouchableOpacity>
        </View>
        <Animated.View style={{
          flex: 1,
          borderRadius,
          transform: [{scale}],
          overflow: 'hidden',
        }}>
          <DrawerItemList {...props} />
        </Animated.View>
        <View style={{alignSelf:"flex-start", margin:30,alignItems:"flex-start"}}>
          <TouchableOpacity style ={{flexDirection:'row',alignItems:"center"}} onPress={()=>dispatch(userLogout())}>
            <Icon name="logout" size={25} style={{ color:"white"}} />
            <Text style={{sontSize:20,color:"white",}}>Logout</Text>
          </TouchableOpacity>
        </View>
      </DrawerContentScrollView>
    );
  }