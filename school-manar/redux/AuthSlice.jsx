import AsyncStorage from "@react-native-async-storage/async-storage";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

export const addToken = createAsyncThunk(
    'addtoken',
    async ()=>{
        console.log('token')
       let result =  await AsyncStorage.getItem('data') ;
       result=JSON.parse(result)
       //console.log(result.token)
       return result.token
    }
)
export const login = createAsyncThunk(
    'login',
    async ({name,password})=>{
        
       const res= await fetch("https://b17e-196-89-236-37.ngrok-free.app/api/auth/login",{
          method:"POST",
          headers: {
           'Content-Type': 'application/json'
         },
         body:JSON.stringify({
           "name":name,
           "password":password
         })
        })
        console.log(res);
        //return res.json();
        console.log(name);
    }
)
const AuthSlice=createSlice({
    name:"auth",
    initialState:{
        
        user:"",
        token:"",
        
        loading:false,
        error:null,
        user:null
    },
    reducers:{
        setData:(state,action)=>{
            state.user=action.payload.user;
            state.token=action.payload.access_token;
        },
        userLogout:(state,action)=>{
            state.user=null;
            state.error=null;
            state.loading=false;
            state.token=null;
            AsyncStorage.clear();
        }
    },
    extraReducers:(builder)=>{
        //addToken
        builder.addCase(addToken.fulfilled,(state,action)=>{
            if(action.payload){
                state.user =action.payload.user;
                state.token=action.payload.access_token;
            }
        });
        login
        builder.addCase(login.pending,(state,action)=>{
            state.loading =true;
            state.user=null;
            state.token=null;
            state.error=null;
        });
        builder.addCase(login.fulfilled,async(state,action)=>{
            
            if(action.payload.error){
                state.error="username or password invalid";
            }
            else if(action.payload.name||action.payload.password){
                state.error="field required";
            }
            else if (action.payload.user)  {
                state.user =action.payload.user;
                state.token=action.payload.access_token;
                await AsyncStorage.setItem('data',JSON.stringify(action.payload))
            }
            state.loading =false;
        });
        builder.addCase(login.rejected,(state,action)=>{
            state.error =action.payload;
            state.loading =false;
        });
        
    }

    
})
export const {setData,userLogout}=AuthSlice.actions
export default AuthSlice.reducer