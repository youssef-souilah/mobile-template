import { combineReducers, configureStore } from "@reduxjs/toolkit";
import AuthSlice from "./AuthSlice";

const reducers=combineReducers({
    auth:AuthSlice
})
const store=configureStore({
    reducer:reducers
})
export default store