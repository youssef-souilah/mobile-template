import * as React from 'react';
import { View, Text ,StatusBar,TouchableOpacity,Image} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
  useDrawerProgress,
  useDrawerStatus,
} from '@react-navigation/drawer';
import Animated from 'react-native-reanimated';
import profile from '../assets/login.png';
import CustomDrawerContent from '../customs/CustomDrawerContent';
import ExampleNavigation from '../navigations/ExampleNavigation';
import { ActivityIndicator } from 'react-native-paper';
import Loading from '../pages/Loading';

function Article() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Article Screen hh</Text>
      </View>
    );
  }

const Drawer = createDrawerNavigator();
export default function ProfStack(props) {

    const [loading,setLoading]=React.useState(true);

    React.useEffect(()=>{
        setTimeout(()=>{
            setLoading(false);
        },1500);
    },[loading]);
    return ( loading?<Loading  />:
      <Drawer.Navigator
        screenOptions={options}
        useLegacyImplementation
        drawerContent={(props) => <CustomDrawerContent {...props} />}
      >
        <Drawer.Screen name="Home" options={{
            drawerIcon: ({color}) => (
                  <Icon name="home" size={25} style={{marginRight: -20, color}} />
            ),
          }} component={ExampleNavigation } />
        <Drawer.Screen name="Article" options={{
            drawerIcon: ({color}) => (
                  <Icon name="newspaper" size={25} style={{marginRight: -20, color}} />
            ),
          }}   component={Article} />
        <Drawer.Screen name="Notifications" options={{
            drawerIcon: ({color}) => (
                  <Icon name="bell" size={25} style={{marginRight: -20, color}} />
            ),
          }}   component={Article} />
        <Drawer.Screen name="News" options={{
            drawerIcon: ({color}) => (
                  <Icon name="newspaper" size={25} style={{marginRight: -20, color}} />
            ),
          }}   component={Article} />
        <Drawer.Screen name="Tests" options={{
            drawerIcon: ({color}) => (
                  <Icon name="image" size={25} style={{marginRight: -20, color}} />
            ),
          }}   component={Article} />
        <Drawer.Screen name="Links" options={{
            drawerIcon: ({color}) => (
                  <Icon name="link" size={25} style={{marginRight: -20, color}} />
            ),
          }}   component={Article} />
      </Drawer.Navigator>
    );
  }

const options={
    headerShown: false,
    headerStyle: {
      height: 60, 
    },
    drawerType: 'slide',
    drawerStyle: {
      width: 250,
      backgroundColor: "#0693e3",
    },
    overlayColor: null,
    drawerLabelStyle: {
      fontWeight: 'bold',
    },
    drawerActiveTintColor: "#e91e63",
    drawerInactiveTintColor: "white",
    drawerLabelStyle: {
      marginLeft: 2 ,
      fontSize: 20,
    },
    drawerItemStyle: {backgroundColor: "tranparant"},
    sceneContainerStyle: {
      backgroundColor: "white",
      borderRadius: 50,
      color:"blue"
    },
  }