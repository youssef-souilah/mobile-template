import React, { useEffect, useState } from 'react';

import Test from '../pages/Test'
import { View ,Platform,Button,Text} from 'react-native';
// import * as ImagePicker from 'expo-image-picker';
import { SafeAreaView } from 'react-native-safe-area-context';
import { createNativeStackNavigator } from '@react-navigation/native-stack';



const UserStack = () => {
  const Stack=createNativeStackNavigator();
  return(
    <Stack.Navigator>
        <Stack.Screen 
        name="Test" 
        component={Test}
        options={{headerShown: false}}
        />
      </Stack.Navigator>
    
  )
}

export default UserStack;