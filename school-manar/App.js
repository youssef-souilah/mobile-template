import{ Provider, useDispatch, useSelector } from 'react-redux';
import store from './redux/store';
import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AuthStack from './stacks-screens/AuthStack';
import UserStack from './stacks-screens/UserStack';
import AdminStack from './stacks-screens/AdminStack';
import { addToken } from './redux/AuthSlice';
import ProfStack from './stacks-screens/ProfStack';
//import { createNativeStackNavigator } from '@react-navigation/native-stack';

//AsyncStorage.clear()
function App() {
  const {user} = useSelector(state => state.auth)
  const dispatch = useDispatch()
  useEffect(()=>{
    dispatch(addToken())
    ///console.log(data);
  },[user])
  return (
    <NavigationContainer>

    {
      user? 
      user.is_teacher==1?
        <ProfStack/>:<AuthStack/>
        // data.user.is_teacher===1?
        // <AuthStack/>:null
      :<AuthStack />  
    }
   
    </NavigationContainer>
  );
}

export default ()=>{
  return (
    <Provider store={store}>
      <App/>
     </Provider>
  )
}



